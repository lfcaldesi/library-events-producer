POST WITH-NULL-LIBRARY-EVENT-ID
---------------------
curl -i \
-d '{"libraryEventId":null,"book":{"bookId":123,"bookName":"Book1","bookAuthor":"Author1"}}' \
-H "Content-Type: application/json" \
-X POST http://localhost:8080/v1/libraryevent


PUT WITH-ID
---------------------
curl -i \
-d '{"libraryEventId":123,"book":{"bookId":123,"bookName":"Book1","bookAuthor":"Author1"}}' \
-H "Content-Type: application/json" \
-X PUT http://localhost:8080/v1/libraryevent


PUT WITHOUT-ID
---------------------
curl -i \
-d '{"libraryEventId":null,"book":{"bookId":123,"bookName":"Book1","bookAuthor":"Author1"}}' \
-H "Content-Type: application/json" \
-X PUT http://localhost:8080/v1/libraryevent
